#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

#define WATERTHRESHOLD 400
#define SUCCESSIVETHRESHOLD 3
#define CHECKDELAY 2000

#define PUMPPOWER 50

#define S3 10
#define D1 5

// Set WIFI SSID and PASSWORD
#define WIFI_SSID       "<WIFI SSID>"
#define WIFI_PASSWORD   "<WIFI PASSWORD>"

// Set Pushover.net fields
#define PUSHOVER_TOKEN  "<Pushover Token>"
#define PUSHOVER_USER   "<Pushover User>"


unsigned int count = 0;
boolean done = false;

void setup() {
  WiFi.mode(WIFI_OFF);
  
  pinMode(A0, INPUT);
  
  pinMode(S3, OUTPUT);
  digitalWrite(S3, LOW);
  
  pinMode(D1, OUTPUT);
  analogWriteFreq(1000);
  analogWrite(D1, PUMPPOWER);

  Serial.begin(9600);
}

void pushAlert() {
  BearSSL::WiFiClientSecure client;
  HTTPClient https;
  
  Serial.println("Attempting to push alert.");
  
  WiFi.mode(WIFI_STA);
  WiFi.softAPdisconnect(true);

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  Serial.print("Connecting:");
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("OK");

  //Disable Cert Pinning
  client.setInsecure();

  int httpCode = 0;
  bool successPost = false;

  String messageBody = "token=" + String(PUSHOVER_TOKEN);
  messageBody += "&user=" + String(PUSHOVER_USER);
  messageBody += "&title=Cat%20Water%20Alert";
  messageBody += "&message=Time%20to%20Attend%20to%20Cat%20Water%20Bowl";
  messageBody += "&sound=magic";

  for(int safety = 0; safety < 10; safety++) {  //Only attempt 10 times, if still failed, stop hitting the service
    if (https.begin(client, "https://api.pushover.net/1/messages.json")) {
      https.setUserAgent("Cat Water Monitor");
      https.setTimeout(3000);
      https.addHeader("Content-Type", "application/x-www-form-urlencoded");  
      int httpCode = https.POST(messageBody);
      https.end();

      if(httpCode == 200)
      {
        Serial.println("Alert successfully pushed.");
        break;
      }
      else
        delay(3000); //Sometimes errors occur so wait three seconds and try again.
    }
  }
  
  WiFi.mode(WIFI_OFF);
}

void loop() {
  if(!done)
  {
    //Turn on power to measure
    digitalWrite(S3, HIGH);
    unsigned int measure = analogRead(A0);
    //Turn off pin to prevent oxidation
    digitalWrite(S3, LOW);
    
    if(measure <= WATERTHRESHOLD)
    {
      //We are out of water
      Serial.println("We are out of water.");
      
      count++;

      if(count >= SUCCESSIVETHRESHOLD)
      {
        Serial.println("Confirmed we are out of water.  Running Shutdown + Alert Code.");
        
        //shut off water
        digitalWrite(D1, LOW);
        
        pushAlert();
        
        done = true;
      }
    }
    else
    {
      //We are in the water
      Serial.println("We are detecting water.");
      
      count = 0;
    }
    
  }

  delay(CHECKDELAY);
}
